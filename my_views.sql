CREATE VIEW ALL_WORKERS AS
SELECT 
    last_name AS lastname,
    first_name AS firstname,
    age,
    first_day AS start_date
FROM 
    WORKERS_FACTORY_1
WHERE 
    (last_day IS NULL OR last_day >= SYSDATE)
UNION ALL
SELECT 
    last_name AS lastname,
    first_name AS firstname,
    NULL AS age,  -- Supposant que l'âge n'est pas disponible dans WORKERS_FACTORY_2
    start_date
FROM 
    WORKERS_FACTORY_2
WHERE 
    (end_date IS NULL OR end_date >= SYSDATE)
ORDER BY 
    start_date DESC;
   
   
   CREATE VIEW ALL_WORKERS_ELAPSED AS
SELECT 
    lastname,
    firstname,
    age,
    start_date,
    TRUNC(SYSDATE) - TRUNC(start_date) AS days_elapsed
FROM 
    ALL_WORKERS;

   
   CREATE VIEW BEST_SUPPLIERS AS
SELECT 
    s.name,
    SUM(sp.quantity) AS total_pieces
FROM 
    SUPPLIERS s
JOIN 
    SUPPLIERS_BRING_TO_FACTORY_1 sp ON s.supplier_id = sp.supplier_id
GROUP BY 
    s.name
HAVING 
    SUM(sp.quantity) > 1000
ORDER BY 
    total_pieces DESC;

   
   CREATE VIEW ROBOTS_FACTORIES AS
SELECT 
    r.id AS robot_id,
    r.model AS robot_model,
    f.id AS factory_id,
    f.main_location AS factory_location
FROM 
    ROBOTS r
JOIN 
    ROBOTS_FROM_FACTORY rf ON r.id = rf.robot_id
JOIN 
    FACTORIES f ON rf.factory_id = f.id;

