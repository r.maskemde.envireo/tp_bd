CREATE OR REPLACE TRIGGER trg_all_workers_elapsed_bi
INSTEAD OF INSERT ON ALL_WORKERS_ELAPSED
FOR EACH ROW
BEGIN
    INSERT INTO WORKERS_FACTORY_1 (first_name, last_name, age, first_day)
    VALUES (:NEW.firstname, :NEW.lastname, :NEW.age, :NEW.start_date);
EXCEPTION
    WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001, 'Only insert operations are allowed.');
END;
CREATE OR REPLACE TRIGGER trg_after_robot_insert
AFTER INSERT ON ROBOTS
FOR EACH ROW
BEGIN
    INSERT INTO AUDIT_ROBOT (robot_id, created_at)
    VALUES (:NEW.id, SYSDATE);
END;


CREATE OR REPLACE TRIGGER trg_before_robots_factories_modify
BEFORE INSERT OR UPDATE OR DELETE ON ROBOTS_FACTORIES
DECLARE
    factory_count NUMBER;
    workers_table_count NUMBER;
BEGIN
    -- Compter le nombre d'usines
    SELECT COUNT(*) INTO factory_count FROM FACTORIES;
    
    -- Compter le nombre de tables WORKERS_FACTORY_<N> (Requiert des droits DBA pour exécuter)
    SELECT COUNT(*)
    INTO workers_table_count
    FROM all_tables
    WHERE table_name LIKE 'WORKERS_FACTORY_%';

    IF factory_count != workers_table_count THEN
        RAISE_APPLICATION_ERROR(-20002, 'Number of FACTORIES does not match the number of WORKERS_FACTORY tables.');
    END IF;
END;

